const ps = require('ps-node');

const yourProcessFilename = 'node_filename.js'

async function run(){
    if(await checkIfThisProcessRunning()){
      console.log('End: Running morethan 1 process')
      process.exit(1)
    }
}


async function checkIfThisProcessRunning(){

    return new Promise(function(resolve, reject) {
      ps.lookup({
        command: 'node',
        arguments: `${yourProcessFilename}`,
        psargs: 'ux'
        }, function(err, resultList ) {
        if (err) {
          reject(err)
        }
    
        resultList.forEach(function( proc ){
            if( proc){
                console.log( 'PID: %s, COMMAND: %s, ARGUMENTS: %s', proc.pid, proc.command, proc.arguments );
            }
        });
    
        if(resultList.length > 1){
          resolve(true)
        } else {
          resolve(false)
        }
      });
    });
  }