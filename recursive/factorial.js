function factorial(x,result=x){
    if(x===1){
        //console.log(result)
        return result
    }else{
        //console.log(`x: ${x} result: ${result}`)
        return factorial(x-1,result*(x-1))
    }
}


// แบบที่สองไม่ดี จะทำให้เกิด Stack Overflow ได้
function factorial2(x){
    if(x===1){
        //console.log(result)
        return 1
    }else{
        //console.log(`x: ${x} result: ${result}`)
        return x*factorial2(x-1)
    }
}

console.log(factorial(1000))

