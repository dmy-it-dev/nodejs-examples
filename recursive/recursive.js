var mysql      = require('mysql');  // อย่าลืม  Install library ก่อน: npm install mysql
var mySQLConfig = require('../config/mysql_localhost_config.js');

mySQLconn = mysql.createConnection( {
    host          : mySQLConfig.host,
    user          : mySQLConfig.user,
    password      : mySQLConfig.password,
    database      : mySQLConfig.database,
    timeout       : 6000
  });

mySQLconn.connect();   

let arr = [0,1,2,3,4,5,6,7,8,9]

function insertMySQL(item){
    // Table: test มี field เดียวชื่อ id
    let sql = `insert into test(id) values(${item})`
    return mySQLconn.query(sql, function (error, results, fields) {
        if (error){
            console.log(error)
        } 
        mySQLconn.end; // commit data ยังไม่ปิด connection
        console.log(results)  
        return 
    });    
}

function recursiveLoop(index,p_arr){
    if(index ==  p_arr.length){
        mySQLconn.end() // ปิด connection
        return p_arr[index-1]
    }else{
        insertMySQL(p_arr[index])
        return recursiveLoop(index+1,p_arr)
    }
}

let i = recursiveLoop(0,arr)


